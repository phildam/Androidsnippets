public static class ArrayAdapterExt extends ArrayAdapter<Country> {

        private List<Country> countries = new ArrayList<>();
        private Context context;
        private int mDropDownResource;
        private  LayoutInflater inflater;
        private int mResources;

        public ArrayAdapterExt(@NonNull Context context, int resource, List<Country> countries) {
            super(context, resource);
            this.countries = countries;
            this.context = context;
            inflater =LayoutInflater.from(this.context);
            mResources = mDropDownResource = resource;


        }

        public ArrayAdapterExt(@NonNull Context context, int resource, int textViewResourceId) {
            super(context, resource, textViewResourceId);

        }


        @Override
        public void add(@Nullable Country object) {
            if(countries != null) countries.add(object);
        }


        @Override
        public void insert(@Nullable Country object, int index) {
            if(countries != null) countries.add(index,object);
        }

        @Override
        public void remove(@Nullable Country object) {
            if(countries != null) countries.remove(object);
        }

        @Override
        public void clear() {
            if(countries != null) countries.clear();

        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void setNotifyOnChange(boolean notifyOnChange) {
            super.setNotifyOnChange(notifyOnChange);
        }


        @Override
        public int getCount() {
            if(countries != null){
                return countries.size();
            }
            return 0;
        }

        @Nullable
        @Override
        public Country getItem(int position) {
            return countries.get(position);
        }

        @Override
        public int getPosition(@Nullable Country item) {
            return countries.indexOf(item);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //return super.getView(position, convertView, parent);

            return createViewFromResource(inflater, position, convertView, parent, mResources);
        }

        private @NonNull View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                                     @Nullable View convertView, @NonNull ViewGroup parent, int resource) {
            final View view;
            final TextView text;
            final ImageView imageView;
            final TextView countryCode;
            Country country;

            if (convertView == null) {
                view = inflater.inflate(R.layout.country_view, parent, false);
            } else {
                view = convertView;
            }

            text = (TextView)view.findViewById(R.id.textView2);
            imageView = (ImageView)view.findViewById(R.id.imageView4);
            countryCode = (TextView) view.findViewById(R.id.ccode);
            try{
                country = countries.get(position);
                text.setText(country.getCountry());
                countryCode.setText(country.getCountryCode());
                imageView.setImageResource(country.getFlagID() == 0 ? R.drawable.com_facebook_favicon_blue : country.getFlagID());
            }catch (Exception e){
                Log.d("Error", "error: "+e.getMessage());

            }
            return view;
        }


        @Override
        public View getDropDownView(int position, @Nullable View convertView,
                                    @NonNull ViewGroup parent) {
             //inflater = mDropDownInflater == null ? mInflater : mDropDownInflater;
            return createViewFromResource(inflater, position, convertView, parent, mDropDownResource);
        }


    }